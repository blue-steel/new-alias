# new-alias

## Problem

The "alias" command creates a shortcut that will execute the specified commands when a keyword is entered into the terminal

- the alias command only saves this shortcut for the current terminal session.
- aliases must be saved in the shell rc file to persist across sessions

## Solution

### "new-alias" is a terminal command that will save an alias for all new terminal sessions

- aliases are saved in the ~/.alias file
- the ~/.alias file is source in the zshrc file for all new terminals

## Set up

1. Copy the .new-alias file to your home directory
2. run these commands in the terminal

```
touch ~/.aliases
echo "source ~/.new-alias" >> ~/.zshrc
```

## Usage

new-alias [options][alias][instructions]

### options:
| option | description |
|:----|:----|
| -d, --delete | delete [alias], will delete for all future terminal sessions
| -l, --list | list all aliases saved in the terminal |
| -h, --help | show help menu |

- To save a new alias call the new alias command and pass a name and instructions.
- The instructions argument needs to be in quotes if there are any spaces in the command.
- To put multiple commands in the same alias, separate commands with ";"
- To execute an alias symply type the alias name into the terminal and press enter

```
# save
new-alias newAlias "cd change to new directory; open thisFile.txt;"

# use
newAlias

#list
alias

# delete
new-alias --delete oldAlias
```
